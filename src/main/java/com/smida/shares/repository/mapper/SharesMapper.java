package com.smida.shares.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import com.smida.shares.domain.Shares;
import com.smida.shares.domain.SharesStatus;
import org.springframework.jdbc.core.RowMapper;

public class SharesMapper implements RowMapper<Shares> {

  @Override
  public Shares mapRow(ResultSet rs, int rowNum) throws SQLException {
    Shares shares = new Shares();
    shares.setId(UUID.fromString(rs.getString("id")));
    shares.setComment(rs.getString("comment"));
    shares.setAmount(rs.getInt("amount"));
    shares.setDate(rs.getTimestamp("date").toInstant());
    shares.setEDRPOU(rs.getInt("edrpou"));
    shares.setParValue(rs.getInt("par_value"));
    shares.setStatus(SharesStatus.valueOf(rs.getString("shares_status")));
    shares.setTotalParValue(rs.getInt("total_par_value"));
    return shares;
  }

}
