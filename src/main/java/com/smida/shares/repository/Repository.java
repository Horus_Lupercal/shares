package com.smida.shares.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Pageable;

public interface Repository<T> {

  Optional<T> get(UUID id);

  Optional<T> save(T entity);

  List<T> getAll(Pageable pageable);

  boolean delete(UUID id);
}
