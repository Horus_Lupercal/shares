package com.smida.shares.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.UUID;
import javax.sql.DataSource;
import com.smida.shares.domain.Filter;
import com.smida.shares.domain.Shares;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.repository.Repository;
import com.smida.shares.repository.mapper.SharesMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;

@Component
public class SharesRepository implements Repository<Shares> {

  private JdbcTemplate jdbcTemplate;
  private SharesMapper mapper;

  @Autowired
  public SharesRepository(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
    mapper = new SharesMapper();
  }

  @Override
  public Optional<Shares> get(UUID id) {
    try {
      Shares shares = jdbcTemplate.queryForObject(
          "SELECT id, comment, edrpou, total_par_value, par_value, amount, shares_status, date FROM shares WHERE id = ?;",
          mapper, id);
      return Optional.of(shares);
    } catch (Exception e) {
      return Optional.empty();
    }
  }

  @Override
  public Optional<Shares> save(Shares shares) {
    UUID id = UUID.randomUUID();
    String sql =
        "INSERT INTO shares(id, comment, edrpou, total_par_value, par_value, amount, shares_status, date) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    jdbcTemplate.update(new PreparedStatementCreator() {
      public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
        PreparedStatement pst = con.prepareStatement(sql);
        pst.setObject(1, id);
        pst.setString(2, shares.getComment());
        pst.setInt(3, shares.getEDRPOU());
        pst.setInt(4, shares.getTotalParValue());
        pst.setInt(5, shares.getParValue());
        pst.setInt(6, shares.getAmount());
        pst.setString(7, shares.getStatus().toString());
        pst.setTimestamp(8, Timestamp.from(shares.getDate()));
        return pst;
      }
    });
    shares.setId(id);
    return Optional.of(shares);
  }

  public Optional<Shares> update(Shares update) {
    int count = jdbcTemplate.update(
        "UPDATE shares SET comment=?, edrpou=?, total_par_value=?, par_value=?, amount=?, shares_status=?, date=? WHERE id=?;",
        update.getComment(), update.getEDRPOU(), update.getTotalParValue(), update.getParValue(),
        update.getAmount(), update.getStatus().toString(), update.getDate(), update.getId());

    if (count > 0) {
      return Optional.of(update);
    }
    return Optional.empty();
  }

  @Override
  public boolean delete(UUID id) {
    int count = jdbcTemplate.update("UPDATE shares SET shares_status=? WHERE id=?;",
        SharesStatus.DELETED.toString(), id);

    return count > 0;
  }

  @Override
  public List<Shares> getAll(Pageable pageable) {
    int size = pageable.getPageSize();
    long offset = pageable.getOffset();
    return jdbcTemplate.query(
        "SELECT id, comment, edrpou, total_par_value, par_value, amount, shares_status, date FROM shares LIMIT "
            + size + " OFFSET " + offset + ";",
        mapper);
  }

  public List<Shares> filter(Filter filter, Pageable pageable) {
    int size = pageable.getPageSize();
    long offset = pageable.getOffset();

    List<Object> listParameters = new ArrayList<>();

    String selectSql =
        "SELECT id, comment, edrpou, total_par_value, par_value, amount, shares_status, date FROM shares where "
            + createQuery(filter, listParameters) + " LIMIT " + size + " OFFSET " + offset + ";";

    return jdbcTemplate.query(selectSql, mapper, listParameters.toArray());
  }

  private String createQuery(Filter filter, List<Object> parametes) {
    StringJoiner result = new StringJoiner(" AND ");
    if (filter.getAmount() != null) {
      result.add("amount = ?");
      parametes.add(filter.getAmount());
    }

    if (filter.getComment() != null) {
      result.add("comment = ?");
      parametes.add(filter.getComment());
    }

    if (filter.getDate() != null) {
      result.add("date = ?");
      parametes.add(filter.getDate());
    }

    if (filter.getEDRPOU() != null) {
      result.add("edrpou = ?");
      parametes.add(filter.getEDRPOU());
    }

    if (filter.getParValue() != null) {
      result.add("par_value = ?");
      parametes.add(filter.getParValue());
    }

    if (filter.getStatus() != null) {
      result.add("shares_status = ?");
      parametes.add(filter.getStatus().toString());
    }

    if (filter.getTotalParValue() != null) {
      result.add("total_par_value = ?");
      parametes.add(filter.getTotalParValue());
    }
    return result.toString();
  }
}
