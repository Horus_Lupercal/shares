package com.smida.shares.repository.repositoryimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.sql.DataSource;
import com.smida.shares.domain.SharesChanges;
import com.smida.shares.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;

@Component
public class SharesChangesRepository implements Repository<SharesChanges> {

  private JdbcTemplate jdbcTemplate;
  private BeanPropertyRowMapper<SharesChanges> mapper;

  @Autowired
  public SharesChangesRepository(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
    mapper = new BeanPropertyRowMapper<>(SharesChanges.class);
  }

  @Override
  public Optional<SharesChanges> get(UUID id) {
    SharesChanges shares = jdbcTemplate.queryForObject(
        "SELECT id, old_value, field_name, new_value, edrpou, shares_id FROM shares_changes WHERE id = ?;",
        mapper, id);
    return Optional.of(shares);
  }

  @Override
  public Optional<SharesChanges> save(SharesChanges sharesChanges) {
    UUID id = UUID.randomUUID();
    String sql =
        "INSERT INTO shares_changes(id, old_value, field_name, new_value, edrpou, shares_id) VALUES (?, ?, ?, ?, ?, ?);";
    jdbcTemplate.update(new PreparedStatementCreator() {
      public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
        PreparedStatement pst = con.prepareStatement(sql);
        pst.setObject(1, id);
        pst.setString(2, sharesChanges.getOldValue());
        pst.setString(3, sharesChanges.getFieldName());
        pst.setString(4, sharesChanges.getNewValue());
        pst.setInt(5, sharesChanges.getEDRPOU());
        pst.setObject(6, sharesChanges.getSharesId());

        return pst;
      }
    });
    sharesChanges.setId(id);
    return Optional.of(sharesChanges);
  }

  @Override
  public boolean delete(UUID id) {
    int count = jdbcTemplate.update("DELETE FROM shares_changes WHERE id=?;", id);

    return count > 0;
  }

  @Override
  public List<SharesChanges> getAll(Pageable pageable) {
    int size = pageable.getPageSize();
    long offset = pageable.getOffset();

    return jdbcTemplate.query(
        "SELECT id, old_value, field_name, new_value, edrpou, shares_id FROM shares_changes LIMIT"
            + size + " OFFSET " + offset + ";",
        mapper);
  }

}
