package com.smida.shares.controller;

import java.util.List;
import java.util.UUID;
import com.smida.shares.domain.Filter;
import com.smida.shares.dto.SharesDTO;
import com.smida.shares.service.SharesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SharesController {

  @Autowired
  private SharesService service;

  @GetMapping("/{id}")
  public SharesDTO getShares(@PathVariable UUID id) {
    return service.get(id);
  }

  @PostMapping
  public SharesDTO createShares(@RequestBody SharesDTO dto) {
    return service.create(dto);
  }

  @PatchMapping("/{id}")
  public SharesDTO updateShares(@RequestBody SharesDTO dto) {
    return service.update(dto);
  }

  @DeleteMapping("/{id}")
  public boolean deleteShares(@PathVariable UUID id) {
    return service.delete(id);
  }

  @GetMapping
  public List<SharesDTO> getAll(@RequestParam("page") int pageIndex,
      @RequestParam("size") int pageSize) {
    if (pageSize == 0) {
      pageSize = 10;
    }
    return service.getAll(PageRequest.of(pageIndex, pageSize));
  }

  @GetMapping("/filter")
  public List<SharesDTO> getSharesDTO(@RequestBody Filter filter,
      @RequestParam("page") int pageIndex, @RequestParam("size") int pageSize) {
    if (pageSize == 0) {
      pageSize = 10;
    }
    return service.filter(filter, PageRequest.of(pageIndex, pageSize));
  }
}
