package com.smida.shares.controller;

import java.util.List;
import java.util.UUID;
import com.smida.shares.dto.SharesChangesDTO;
import com.smida.shares.service.SharesChangesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/shareschanges")
public class SharesChangesController {

  @Autowired
  private SharesChangesService service;

  @GetMapping("/{id}")
  public SharesChangesDTO getShares(@PathVariable UUID id) {
    return service.get(id);
  }

  @PostMapping
  public SharesChangesDTO createShares(@RequestBody SharesChangesDTO dto) {
    return service.create(dto);
  }

  @DeleteMapping("/{id}")
  public boolean deleteShares(@PathVariable UUID id) {
    return service.delete(id);
  }

  @GetMapping
  public List<SharesChangesDTO> getAll(@RequestParam("page") int pageIndex,
      @RequestParam("size") int pageSize) {
    if (pageSize == 0) {
      pageSize = 10;
    }
    return service.getAll(PageRequest.of(pageIndex, pageSize));
  }
}
