package com.smida.shares.exception;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

  public EntityNotFoundException(Class entityClass, UUID id) {
    super(String.format("Entity %s with id=%s is not found!", entityClass.getSimpleName(), id));
  }
}
