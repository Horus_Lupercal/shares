package com.smida.shares.exception;

public class EntitySaveException extends RuntimeException {

  public EntitySaveException(Class entityClass) {
    super(String.format("Entity %s is not save!", entityClass.getSimpleName()));
  }
}
