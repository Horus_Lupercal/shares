package com.smida.shares.onstart;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smida.shares.controller.SharesController;
import com.smida.shares.dto.SharesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class Startup implements ApplicationListener<ContextRefreshedEvent> {

  @Autowired
  private SharesController controller;

  @Autowired
  private ObjectMapper mapper;

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    try {
      Path path = Paths.get(getClass().getClassLoader().getResource("database\\data").toURI());

      for (File file : path.toFile().listFiles()) {
        if (!file.isDirectory()) {
          SharesDTO dto = mapper.readValue(file, SharesDTO.class);
          controller.createShares(dto);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}

