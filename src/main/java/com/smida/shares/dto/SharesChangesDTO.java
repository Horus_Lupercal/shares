package com.smida.shares.dto;

import java.util.UUID;
import lombok.Data;

@Data
public class SharesChangesDTO {
  private UUID id;
  private String oldValue;
  private String fieldName;
  private String newValue;
  private Integer EDRPOU;
  private UUID sharesId;
}
