package com.smida.shares.dto;

import java.time.Instant;
import java.util.UUID;
import com.smida.shares.domain.SharesStatus;
import lombok.Data;

@Data
public class SharesDTO {
  private UUID id;
  private String comment;
  private Integer EDRPOU;
  private Integer amount;
  private Integer totalParValue;
  private Integer parValue;
  private Instant date;
  private SharesStatus status;
}
