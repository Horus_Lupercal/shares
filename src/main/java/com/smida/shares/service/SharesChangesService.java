package com.smida.shares.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import com.smida.shares.domain.SharesChanges;
import com.smida.shares.dto.SharesChangesDTO;
import com.smida.shares.exception.EntityNotFoundException;
import com.smida.shares.exception.EntitySaveException;
import com.smida.shares.repository.repositoryimpl.SharesChangesRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SharesChangesService {

  @Autowired
  private SharesChangesRepository repository;

  @Autowired
  private ServiceHelper helper;

  @Autowired
  private ModelMapper modelMapper;

  public SharesChangesDTO get(UUID id) {
    SharesChanges response =
        repository.get(id).orElseThrow(() -> new EntityNotFoundException(SharesChanges.class, id));
    return modelMapper.map(response, SharesChangesDTO.class);
  }

  public SharesChangesDTO create(SharesChangesDTO dto) {
    SharesChanges sharesChanges = modelMapper.map(dto, SharesChanges.class);
    SharesChanges response = repository.save(sharesChanges)
        .orElseThrow(() -> new EntitySaveException(SharesChanges.class));
    return modelMapper.map(response, SharesChangesDTO.class);
  }

  public Boolean delete(UUID id) {
    return repository.delete(id);
  }

  public List<SharesChangesDTO> getAll(Pageable pageable) {
    return sharesChangeToSharesChangeDTO(repository.getAll(pageable));
  }

  private List<SharesChangesDTO> sharesChangeToSharesChangeDTO(List<SharesChanges> sharesChages) {

    List<SharesChangesDTO> sharesDto =
        sharesChages.stream().map(source -> modelMapper.map(source, SharesChangesDTO.class))
            .collect(Collectors.toList());

    return sharesDto;
  }
}
