package com.smida.shares.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import com.smida.shares.domain.Filter;
import com.smida.shares.domain.Shares;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.dto.SharesDTO;
import com.smida.shares.exception.EntityNotFoundException;
import com.smida.shares.exception.EntitySaveException;
import com.smida.shares.repository.repositoryimpl.SharesRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SharesService {

  @Autowired
  private SharesRepository repository;

  @Autowired
  private ServiceHelper helper;

  @Autowired
  ModelMapper modelMapper;

  public SharesDTO get(UUID id) {
    Shares response =
        repository.get(id).orElseThrow(() -> new EntityNotFoundException(Shares.class, id));
    return modelMapper.map(response, SharesDTO.class);
  }

  public SharesDTO create(SharesDTO dto) {
    dto.setTotalParValue(dto.getParValue() * dto.getAmount());
    Shares shares = modelMapper.map(dto, Shares.class);
    Shares response =
        repository.save(shares).orElseThrow(() -> new EntitySaveException(Shares.class));
    return modelMapper.map(response, SharesDTO.class);
  }

  public SharesDTO update(SharesDTO dto) {
    calculateTotalParValue(dto);
    helper.saveChanges(dto);
    Shares shares = modelMapper.map(dto, Shares.class);
    Shares response = repository.update(shares)
        .orElseThrow(() -> new EntityNotFoundException(Shares.class, dto.getId()));
    return modelMapper.map(response, SharesDTO.class);
  }

  public Boolean delete(UUID id) {
    SharesDTO dto = new SharesDTO();
    dto.setId(id);
    dto.setStatus(SharesStatus.DELETED);
    helper.saveChanges(dto);
    return repository.delete(id);
  }

  public List<SharesDTO> getAll(Pageable pageable) {
    return sharesToSharesDTO(repository.getAll(pageable));
  }

  public List<SharesDTO> filter(Filter filter, Pageable pageable) {
    if (Boolean.TRUE.equals(isEmptyFilter(filter))) {
      return getAll(pageable);
    }
    return sharesToSharesDTO(repository.filter(filter, pageable));
  }

  private Boolean isEmptyFilter(Filter filter) {
    return filter.getAmount() == null || filter.getComment() == null || filter.getDate() == null
        || filter.getEDRPOU() == null || filter.getParValue() == null || filter.getStatus() == null
        || filter.getTotalParValue() == null;
  }

  private List<SharesDTO> sharesToSharesDTO(List<Shares> shares) {

    List<SharesDTO> sharesDto = shares.stream()
        .map(source -> modelMapper.map(source, SharesDTO.class)).collect(Collectors.toList());

    return sharesDto;
  }

  private void calculateTotalParValue(SharesDTO dto) {
    SharesDTO currectdto = get(dto.getId());
    if (dto.getAmount() != null && dto.getParValue() != null) {
      dto.setTotalParValue(dto.getParValue() * dto.getAmount());
    } else if (dto.getAmount() == null && dto.getParValue() != null) {
      dto.setTotalParValue(dto.getParValue() * currectdto.getAmount());
    } else if (dto.getAmount() != null && dto.getParValue() == null) {
      dto.setTotalParValue(currectdto.getParValue() * dto.getAmount());
    }
  }

}
