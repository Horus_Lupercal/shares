package com.smida.shares.service;

import java.util.UUID;
import com.smida.shares.dto.SharesChangesDTO;
import com.smida.shares.dto.SharesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceHelper {

  @Autowired
  private SharesService sharesService;

  @Autowired
  private SharesChangesService sharesChangesService;

  public void saveChanges(SharesDTO update) {
    SharesDTO currectShares = sharesService.get(update.getId());
    createSharesChanges(currectShares, update);
  }

  private void createSharesChanges(SharesDTO currectShares, SharesDTO update) {

    if (update.getComment() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getComment(),
          update.getComment(), currectShares.getId(), currectShares.getEDRPOU(), "comment");
      sharesChangesService.create(dto);
    }

    if (update.getAmount() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getAmount().toString(),
          update.getAmount().toString(), currectShares.getId(), currectShares.getEDRPOU(),
          "amount");
      sharesChangesService.create(dto);
    }

    if (update.getParValue() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getParValue().toString(),
          update.getParValue().toString(), currectShares.getId(), currectShares.getEDRPOU(),
          "parValue");
      sharesChangesService.create(dto);
    }

    if (update.getStatus() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getStatus().toString(),
          update.getStatus().toString(), currectShares.getId(), currectShares.getEDRPOU(),
          "sharesStatus");
      sharesChangesService.create(dto);
    }

    if (update.getDate() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getDate().toString(),
          update.getDate().toString(), currectShares.getId(), currectShares.getEDRPOU(), "date");
      sharesChangesService.create(dto);
    }

    if (update.getDate() != null) {
      SharesChangesDTO dto = createSharesChangesComment(currectShares.getTotalParValue().toString(),
          update.getTotalParValue().toString(), currectShares.getId(), currectShares.getEDRPOU(),
          "TotalParValue");
      sharesChangesService.create(dto);
    }
  }

  private SharesChangesDTO createSharesChangesComment(String oldComment, String newComment,
      UUID sharesId, Integer EDRPOU, String fieldName) {
    SharesChangesDTO sharesChanges = new SharesChangesDTO();
    sharesChanges.setOldValue(oldComment);
    sharesChanges.setNewValue(newComment);
    sharesChanges.setFieldName(fieldName);
    sharesChanges.setSharesId(sharesId);
    sharesChanges.setEDRPOU(EDRPOU);
    return sharesChanges;
  }
}
