package com.smida.shares.domain;

import java.util.UUID;
import lombok.Data;

@Data
public class SharesChanges {
  private UUID id;
  private String oldValue;
  private String fieldName;
  private String newValue;
  private Integer EDRPOU;
  private UUID sharesId;
}
