package com.smida.shares.domain;

public enum SharesStatus {
  ACTIVE, CLOSE, DELETED;
}
