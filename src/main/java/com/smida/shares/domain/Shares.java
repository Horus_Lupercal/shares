package com.smida.shares.domain;

import java.time.Instant;
import java.util.UUID;
import lombok.Data;

@Data
public class Shares {
  private UUID id;
  private String comment;
  private Integer EDRPOU;
  private Integer amount;
  private Integer totalParValue;
  private Integer parValue;
  private Instant date;
  private SharesStatus status;
}
