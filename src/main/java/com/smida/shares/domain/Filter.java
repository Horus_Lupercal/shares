package com.smida.shares.domain;

import java.time.Instant;
import lombok.Data;

@Data
public class Filter {
  private String comment;
  private Integer EDRPOU;
  private Integer amount;
  private Integer totalParValue;
  private Integer parValue;
  private Instant date;
  private SharesStatus status;
}
