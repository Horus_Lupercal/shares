package com.smida.shares.repository;

import java.time.Instant;
import java.util.List;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.Filter;
import com.smida.shares.domain.Shares;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.repository.repositoryimpl.SharesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@Sql(statements = "delete from shares", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesRepositoryTest {

  @Autowired
  private SharesRepository repository;

  @Test
  public void testSaveSharesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    Assertions.assertNotNull(sharesAfterSave.getId());
  }

  @Test
  public void testGetAllSharesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    List<Shares> result = repository.getAll(PageRequest.of(0, 1));

    Assertions.assertTrue(result.size() == 1);
    Assertions.assertEquals(sharesAfterSave, result.get(0));
  }

  @Test
  public void testUpdateSharesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    Shares update = getShares();
    update.setId(sharesAfterSave.getId());
    update.setAmount(10);
    update.setEDRPOU(123121233);
    update.setParValue(12);
    update.setStatus(SharesStatus.CLOSE);
    update.setTotalParValue(10 * 12);
    update.setComment("fddfdffd");
    update.setDate(Instant.now());

    Shares sharesAfterUpdate = repository.update(update).get();

    Assertions.assertEquals(update, sharesAfterUpdate);
  }

  @Test
  public void testGetSharesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    Shares savedShare = repository.get(sharesAfterSave.getId()).get();

    Assertions.assertEquals(savedShare, sharesAfterSave);
  }

  @Test
  public void testDeleteSharesExcectedStatusDeleted() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    boolean isDeleted = repository.delete(shares.getId());

    Assertions.assertTrue(isDeleted);

    Shares sharesAfterDelete = repository.get(sharesAfterSave.getId()).get();
  }

  @Test
  public void testFilterWithAllParameters() {
    Shares shares = getShares();
    Shares sharesAfterSave = repository.save(shares).get();

    Filter filter = new Filter();
    filter.setComment("TTTT");
    filter.setAmount(10);
    filter.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    filter.setEDRPOU(123121233);
    filter.setParValue(12);
    filter.setStatus(SharesStatus.ACTIVE);
    filter.setTotalParValue(10 * 12);

    List<Shares> result = repository.filter(filter, PageRequest.of(0, 1));

    Assertions.assertTrue(result.size() == 1);
    Assertions.assertEquals(sharesAfterSave, result.get(0));
  }

  @Test
  public void testFilterAndPages() {
    Shares firstShares = getShares();
    repository.save(firstShares).get();
    Shares secondShares = getShares();
    secondShares.setEDRPOU(1234);
    repository.save(secondShares).get();

    PageRequest pageRequest = PageRequest.of(0, 1);

    Filter filter = new Filter();
    filter.setComment("TTTT");
    filter.setAmount(10);
    filter.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    filter.setParValue(12);
    filter.setStatus(SharesStatus.ACTIVE);
    filter.setTotalParValue(10 * 12);

    List<Shares> firstResult = repository.filter(filter, pageRequest);

    Assertions.assertTrue(firstResult.size() == 1);

    List<Shares> secondResult = repository.filter(filter, PageRequest.of(0, 2));

    Assertions.assertTrue(secondResult.size() == 2);

    List<Shares> thirdResult = repository.filter(filter, PageRequest.of(0, 10));

    Assertions.assertTrue(secondResult.size() == 2);
  }

  private Shares getShares() {
    Shares shares = new Shares();
    shares.setComment("TTTT");
    shares.setAmount(10);
    shares.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    shares.setEDRPOU(123121233);
    shares.setParValue(12);
    shares.setStatus(SharesStatus.ACTIVE);
    shares.setTotalParValue(10 * 12);
    return shares;
  }
}
