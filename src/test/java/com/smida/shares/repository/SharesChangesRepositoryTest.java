package com.smida.shares.repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.Shares;
import com.smida.shares.domain.SharesChanges;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.repository.repositoryimpl.SharesChangesRepository;
import com.smida.shares.repository.repositoryimpl.SharesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@Sql(statements = {"delete from shares_changes", "delete from shares"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesChangesRepositoryTest {

  @Autowired
  private SharesRepository sharesRepository;

  @Autowired
  private SharesChangesRepository repository;

  @Test
  public void testSaveSharesChangesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = sharesRepository.save(shares).get();

    SharesChanges sharesChanges = getSharesChanges(sharesAfterSave.getId());

    SharesChanges sharesChangesAfterSave = repository.save(sharesChanges).get();

    Assertions.assertNotNull(sharesChangesAfterSave.getId());
  }

  @Test
  public void testGetAllSharesChangesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = sharesRepository.save(shares).get();

    SharesChanges sharesChanges = getSharesChanges(sharesAfterSave.getId());
    SharesChanges sharesChangesAfterSave = repository.save(sharesChanges).get();

    List<SharesChanges> result = repository.getAll(PageRequest.of(0, 1));

    Assertions.assertTrue(result.size() == 1);
    Assertions.assertEquals(sharesChangesAfterSave, result.get(0));
  }

  @Test
  public void testGetSharesChangesExcectedIsOK() {
    Shares shares = getShares();
    Shares sharesAfterSave = sharesRepository.save(shares).get();

    SharesChanges sharesChanges = getSharesChanges(sharesAfterSave.getId());

    SharesChanges sharesChangesAfterSave = repository.save(sharesChanges).get();

    Assertions.assertEquals(sharesChanges, sharesChangesAfterSave);
  }

  @Test
  public void testDeleteSharesChangesExcectedStatusDeleted() {
    Shares shares = getShares();
    Shares sharesAfterSave = sharesRepository.save(shares).get();

    SharesChanges sharesChanges = getSharesChanges(sharesAfterSave.getId());

    SharesChanges sharesChangesAfterSave = repository.save(sharesChanges).get();

    boolean isDeleted = repository.delete(sharesChangesAfterSave.getId());

    Assertions.assertTrue(isDeleted);
  }

  private Shares getShares() {
    Shares shares = new Shares();
    shares.setComment("TTTT");
    shares.setAmount(10);
    shares.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    shares.setEDRPOU(123121233);
    shares.setParValue(12);
    shares.setStatus(SharesStatus.ACTIVE);
    shares.setTotalParValue(10 * 12);
    return shares;
  }

  private SharesChanges getSharesChanges(UUID id) {
    SharesChanges sharesChages = new SharesChanges();
    sharesChages.setEDRPOU(123121233);
    sharesChages.setFieldName("dsdssd");
    sharesChages.setNewValue("dsfsfdsfd");
    sharesChages.setOldValue("sdfsdf");
    sharesChages.setSharesId(id);
    return sharesChages;
  }
}
