package com.smida.shares.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.Filter;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.dto.SharesDTO;
import com.smida.shares.exception.EntityNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@ActiveProfiles("test")
@Sql(statements = {"delete from shares_changes", "delete from shares"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesServiceTest {

  @Autowired
  public SharesService service;

  @Test
  public void testGetSharesExcectedIsOk() {
    SharesDTO dto = getShares();
    SharesDTO save = service.create(dto);

    SharesDTO afterSave = service.get(save.getId());

    Assertions.assertThat(afterSave).isEqualToComparingFieldByField(save);
  }

  @Test
  public void testCreateSharesExcectedIsOk() {
    SharesDTO dto = getShares();
    SharesDTO afterSave = service.create(dto);

    Assertions.assertThat(dto).isEqualToIgnoringGivenFields(afterSave, "id");
  }

  @Test
  public void testPatchSharesExcectedIsOk() {
    SharesDTO dto = getShares();
    SharesDTO afterSave = service.create(dto);

    afterSave.setComment("DSDSDS");
    afterSave.setParValue(8);
    afterSave.setAmount(7);

    SharesDTO afterPutch = service.update(afterSave);

    Assertions.assertThat(afterPutch).isEqualToComparingFieldByField(afterSave);
  }

  @Test
  public void testGetAllSharesExcectedIsOk() {
    SharesDTO dto = getShares();
    SharesDTO afterSave = service.create(dto);

    List<SharesDTO> response = service.getAll(PageRequest.of(0, 1));

    Assertions.assertThat(afterSave).isEqualToComparingFieldByField(response.get(0));
  }

  @Test
  public void testSharesDeleteExcectedStatusDeleted() {
    SharesDTO dto = getShares();
    SharesDTO afterSave = service.create(dto);

    service.delete(afterSave.getId());

    SharesDTO afterDeleted = service.get(afterSave.getId());

    Assertions.assertThat(afterDeleted.getStatus()).isEqualTo(SharesStatus.DELETED);
  }

  @Test
  public void testFilterWithEmptyParametersExcectedAllEntitis() {
    SharesDTO dto = getShares();
    SharesDTO afterSave = service.create(dto);
    Filter filter = new Filter();

    List<SharesDTO> response = service.filter(filter, PageRequest.of(0, 1));

    Assertions.assertThat(afterSave).isEqualToComparingFieldByField(response.get(0));
  }

  @Test
  public void testGetEntitybyWrongIdExcectedException() {
    assertThrows(EntityNotFoundException.class, () -> {
      service.get(UUID.randomUUID());
    });
  }

  private SharesDTO getShares() {
    SharesDTO dto = new SharesDTO();
    dto.setComment("TTTT");
    dto.setAmount(10);
    dto.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    dto.setEDRPOU(123121233);
    dto.setParValue(12);
    dto.setStatus(SharesStatus.ACTIVE);
    dto.setTotalParValue(10 * 12);
    return dto;
  }
}
