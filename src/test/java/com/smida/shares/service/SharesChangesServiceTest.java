package com.smida.shares.service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.dto.SharesChangesDTO;
import com.smida.shares.dto.SharesDTO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@ActiveProfiles("test")
@Sql(statements = {"delete from shares_changes", "delete from shares"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesChangesServiceTest {

  @Autowired
  private SharesService sharesService;

  @Autowired
  private SharesChangesService service;

  @Test
  public void testGetSharesChangesExcectedIsOk() {
    SharesDTO shares = getShares();
    SharesDTO sharesAfterSave = sharesService.create(shares);

    SharesChangesDTO sharesChanges = getSharesChages(sharesAfterSave.getId());

    SharesChangesDTO sharesChangesAfterSave = service.create(sharesChanges);

    SharesChangesDTO responce = service.get(sharesChangesAfterSave.getId());

    Assertions.assertThat(sharesChangesAfterSave).isEqualToComparingFieldByField(responce);
  }

  @Test
  public void testSaveSharesChangesExcectedIsOk() {
    SharesDTO shares = getShares();
    SharesDTO sharesAfterSave = sharesService.create(shares);

    SharesChangesDTO sharesChanges = getSharesChages(sharesAfterSave.getId());

    SharesChangesDTO sharesChangesAfterSave = service.create(sharesChanges);

    Assertions.assertThat(sharesChanges).isEqualToIgnoringGivenFields(sharesChangesAfterSave, "id");
  }

  @Test
  public void testGetAllSharesChangesExcectedIsOk() {
    SharesDTO shares = getShares();
    SharesDTO sharesAfterSave = sharesService.create(shares);

    SharesChangesDTO sharesChanges = getSharesChages(sharesAfterSave.getId());
    SharesChangesDTO sharesChangesAfterSave = service.create(sharesChanges);

    List<SharesChangesDTO> response = service.getAll(PageRequest.of(0, 1));

    Assertions.assertThat(sharesChangesAfterSave).isEqualToComparingFieldByField(response.get(0));
  }

  @Test
  public void testDeletedSharesChagesExcectedIsOk() {
    SharesDTO shares = getShares();
    SharesDTO sharesAfterSave = sharesService.create(shares);

    SharesChangesDTO sharesChanges = getSharesChages(sharesAfterSave.getId());
    SharesChangesDTO sharesChangesAfterSave = service.create(sharesChanges);

    Boolean isDeleted = service.delete(sharesChangesAfterSave.getId());

    Assertions.assertThat(isDeleted).isEqualTo(true);
  }

  @Test
  public void testSaveChagesOfSharesExcectedIsOK() {
    SharesDTO shares = getShares();
    SharesDTO sharesAfterSave = sharesService.create(shares);
    sharesAfterSave.setAmount(111);
    sharesAfterSave.setComment("Comment");
    sharesAfterSave.setDate(Instant.parse("2020-11-30T18:35:24.00Z"));
    sharesAfterSave.setStatus(SharesStatus.CLOSE);
    sharesAfterSave.setParValue(12);
    SharesDTO sharesAfterUpdate = sharesService.update(sharesAfterSave);

    sharesService.delete(sharesAfterUpdate.getId());

    List<SharesChangesDTO> response = service.getAll(PageRequest.of(0, 1));

    Assertions.assertThat(response.size() == 7).isEqualTo(true);
  }

  private SharesDTO getShares() {
    SharesDTO dto = new SharesDTO();
    dto.setComment("TTTT");
    dto.setAmount(10);
    dto.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    dto.setEDRPOU(123121233);
    dto.setParValue(12);
    dto.setStatus(SharesStatus.ACTIVE);
    dto.setTotalParValue(10 * 12);
    return dto;
  }

  private SharesChangesDTO getSharesChages(UUID id) {
    SharesChangesDTO dto = new SharesChangesDTO();
    dto.setEDRPOU(12415);
    dto.setOldValue("ffff");
    dto.setNewValue("wwww");
    dto.setEDRPOU(123121233);
    dto.setFieldName("Field");
    dto.setSharesId(id);
    return dto;
  }
}
