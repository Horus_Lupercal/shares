package com.smida.shares.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.Filter;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.dto.SharesDTO;
import com.smida.shares.service.SharesService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@ActiveProfiles("test")
@Sql(statements = {"delete from shares_changes", "delete from shares"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesControllerTest {

  @Autowired
  private SharesService sharesServicer;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @BeforeEach
  void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  void testGetShareExcectedIsOk() throws Exception {
    SharesDTO read = getSharesDTO();
    SharesDTO afterSave = sharesServicer.create(read);

    String resultJson = mockMvc.perform(get("/{id}", afterSave.getId())).andExpect(status().isOk())
        .andReturn().getResponse().getContentAsString();

    SharesDTO actualShares = objectMapper.readValue(resultJson, SharesDTO.class);
    Assertions.assertThat(actualShares).isEqualToComparingFieldByField(afterSave);
  }

  @Test
  public void testCreateShareExcectedIsOk() throws Exception {
    SharesDTO create = getSharesDTO();

    String resultJson = mockMvc
        .perform(post("/").content(objectMapper.writeValueAsString(create))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    SharesDTO actualShares = objectMapper.readValue(resultJson, SharesDTO.class);
    Assertions.assertThat(actualShares).isEqualToIgnoringGivenFields(create, "id");
  }

  @Test
  public void testPatchSharesExcectedIsOk() throws Exception {
    SharesDTO read = getSharesDTO();

    SharesDTO afterSave = sharesServicer.create(read);
    afterSave.setAmount(12);
    afterSave.setComment("****");
    afterSave.setParValue(54);
    afterSave.setTotalParValue(12 * 54);
    afterSave.setDate(Instant.parse("2228-11-30T18:35:24.00Z"));

    String resultJson = mockMvc
        .perform(patch("/{id}", read.getId()).content(objectMapper.writeValueAsString(afterSave))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    SharesDTO actualShares = objectMapper.readValue(resultJson, SharesDTO.class);
    Assertions.assertThat(actualShares).isEqualToIgnoringGivenFields(afterSave, "id");
  }

  @Test
  public void testGetAllSharesExcectedIsOk() throws Exception {
    SharesDTO read = getSharesDTO();
    SharesDTO afterSave = sharesServicer.create(read);

    String resultJson = mockMvc
        .perform(get("/").param("page", Integer.toString(0)).param("size", Integer.toString(1)))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    List<SharesDTO> actualShares =
        objectMapper.readValue(resultJson, new TypeReference<List<SharesDTO>>() {});

    Assertions.assertThat(actualShares.get(0)).isEqualToComparingFieldByField(afterSave);
  }

  @Test
  public void testDeleteSharesExcectedStatusDeleted() throws Exception {
    SharesDTO read = getSharesDTO();
    SharesDTO afterSave = sharesServicer.create(read);

    mockMvc.perform(delete("/{id}", afterSave.getId())).andExpect(status().isOk());
  }

  @Test
  public void testFilterSharesExcepredIsOk() throws Exception {
    SharesDTO read = getSharesDTO();
    SharesDTO afterSave = sharesServicer.create(read);

    Filter filter = new Filter();
    filter.setComment("TTTT");
    filter.setAmount(10);
    filter.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    filter.setParValue(12);
    filter.setStatus(SharesStatus.ACTIVE);
    filter.setTotalParValue(10 * 12);

    String resultJson = mockMvc
        .perform(get("/filter").param("page", Integer.toString(0))
            .param("size", Integer.toString(1)).content(objectMapper.writeValueAsString(filter))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    List<SharesDTO> actualShares =
        objectMapper.readValue(resultJson, new TypeReference<List<SharesDTO>>() {});

    Assertions.assertThat(actualShares.get(0)).isEqualToComparingFieldByField(afterSave);
  }

  private SharesDTO getSharesDTO() {
    SharesDTO dto = new SharesDTO();
    dto.setId(UUID.randomUUID());
    dto.setComment("TTTT");
    dto.setAmount(10);
    dto.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    dto.setEDRPOU(123121233);
    dto.setParValue(12);
    dto.setStatus(SharesStatus.ACTIVE);
    dto.setTotalParValue(10 * 12);
    return dto;
  }
}
