package com.smida.shares.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smida.shares.config.SpringTestConfiguration;
import com.smida.shares.domain.SharesStatus;
import com.smida.shares.dto.SharesChangesDTO;
import com.smida.shares.dto.SharesDTO;
import com.smida.shares.service.SharesChangesService;
import com.smida.shares.service.SharesService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SpringTestConfiguration.class})
@WebAppConfiguration
@ActiveProfiles("test")
@Sql(statements = {"delete from shares_changes", "delete from shares"},
    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class SharesChangesControllerTest {

  @Autowired
  private SharesChangesService sharesChangesService;

  @Autowired
  private SharesService sharesServicer;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @BeforeEach
  void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
  }

  @Test
  void testGetShareExcectedIsOk() throws Exception {
    SharesDTO shares = getSharesDTO();
    shares = sharesServicer.create(shares);

    SharesChangesDTO read = getSharesChangesDTO(shares.getId());
    SharesChangesDTO afterSave = sharesChangesService.create(read);

    String resultJson = mockMvc.perform(get("/shareschanges/{id}", afterSave.getId()))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    SharesChangesDTO actualShares = objectMapper.readValue(resultJson, SharesChangesDTO.class);
    Assertions.assertThat(actualShares).isEqualToComparingFieldByField(afterSave);
  }

  @Test
  public void testCreateShareExcectedIsOk() throws Exception {
    SharesDTO shares = getSharesDTO();
    shares = sharesServicer.create(shares);

    SharesChangesDTO create = getSharesChangesDTO(shares.getId());

    String resultJson = mockMvc
        .perform(post("/shareschanges").content(objectMapper.writeValueAsString(create))
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    SharesChangesDTO actualShares = objectMapper.readValue(resultJson, SharesChangesDTO.class);
    Assertions.assertThat(actualShares).isEqualToIgnoringGivenFields(create, "id");
  }

  @Test
  public void testGetAllSharesExcectedIsOk() throws Exception {
    SharesDTO shares = getSharesDTO();
    shares = sharesServicer.create(shares);

    SharesChangesDTO read = getSharesChangesDTO(shares.getId());
    SharesChangesDTO afterSave = sharesChangesService.create(read);

    String resultJson = mockMvc
        .perform(get("/shareschanges").param("page", Integer.toString(0)).param("size",
            Integer.toString(1)))
        .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

    List<SharesChangesDTO> actualShares =
        objectMapper.readValue(resultJson, new TypeReference<List<SharesChangesDTO>>() {});

    Assertions.assertThat(actualShares.get(0)).isEqualToComparingFieldByField(afterSave);
  }

  @Test
  public void testDeleteSharesExcectedStatusDeleted() throws Exception {
    SharesDTO shares = getSharesDTO();
    shares = sharesServicer.create(shares);

    SharesChangesDTO read = getSharesChangesDTO(shares.getId());
    SharesChangesDTO afterSave = sharesChangesService.create(read);

    mockMvc.perform(delete("/shareschanges/{id}", afterSave.getId())).andExpect(status().isOk());
  }


  private SharesDTO getSharesDTO() {
    SharesDTO shares = new SharesDTO();
    shares.setComment("TTTT");
    shares.setAmount(10);
    shares.setDate(Instant.parse("2018-11-30T18:35:24.00Z"));
    shares.setEDRPOU(123121233);
    shares.setParValue(12);
    shares.setStatus(SharesStatus.ACTIVE);
    shares.setTotalParValue(10 * 12);
    return shares;
  }

  private SharesChangesDTO getSharesChangesDTO(UUID id) {
    SharesChangesDTO sharesChagesDTO = new SharesChangesDTO();
    sharesChagesDTO.setEDRPOU(123121233);
    sharesChagesDTO.setFieldName("dsdssd");
    sharesChagesDTO.setNewValue("dsfsfdsfd");
    sharesChagesDTO.setOldValue("sdfsdf");
    sharesChagesDTO.setSharesId(id);
    return sharesChagesDTO;
  }
}
